#!/usr/bin/env pythoh3
'''
Cálculo del número ópyimo de árboles.
'''

import sys

def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):

    productions = []
    ...
    return productions

def read_arguments():

    ...
    return base_trees, fruit_per_tree, reduction, min, max


def main():

    ...
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)
    ...
    print(f"Best production: {best_production}, for {best_trees} trees")

if __name__ == '__main__':
    main()
